package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Fruit;

/**
 * Servlet implementation class HealthCheck
 */
@WebServlet("/Fruit82")
public class Fruit82 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Fruit82() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Fruit fruit = new Fruit("いちご", 700);
		HttpSession session = request.getSession();
		session.setAttribute("fruit", fruit);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/show2.jsp");
		dispatcher.forward(request, response);
	}
}

