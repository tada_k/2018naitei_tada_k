package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Employee;

public class EmployeeDAO {
  public List<Employee> findAll() {

    Connection conn = null;
    List<Employee> empList = new ArrayList<Employee>();

    try {
      Class.forName("org.h2.Driver");
      conn = DriverManager.getConnection("jdbc:h2:file:C:/data/example", "sa", "");
      String sql = "SELECT ID, NAME, AGE FROM EMPLOYEE";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      ResultSet rs = pStmt.executeQuery();
      while (rs.next()) {
        String id = rs.getString("ID");
        String name = rs.getString("NAME");
        int age = rs.getInt("AGE");
        Employee employee = new Employee(id, name, age);
        empList.add(employee);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return empList;
  }
  public boolean remove(String id) {
	  Connection conn = null;
	  try{
	      Class.forName("org.h2.Driver");
	      conn = DriverManager.getConnection("jdbc:h2:file:C:/data/example", "sa", "");
	      String sql = "DELETE FROM EMPLOYEE WHERE ID=?";
	      PreparedStatement pStmt = conn.prepareStatement(sql);
	      pStmt.setString(1,id);
	      int r = pStmt.executeUpdate();
	      return (r > 0);
	  } catch (SQLException e) {
	      e.printStackTrace();
	      return false;
	  } catch (ClassNotFoundException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	      return false;
	}finally{
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	          return false;
	        }
	      }
	  }
  }
}