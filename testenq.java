
//package example;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/testenq")
/**
 * Servlet implementation class Servlet
 */



public class Lesson5testenq  extends HttpServlet{
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String name = request.getParameter("name");
		String qtype = request.getParameter("qtype");
		String body = request.getParameter("body");


		String errorMsg = "";

		if(name == null || name.length() == 0){
			errorMsg += "名前が入力されていません<br>";
		}

		if(qtype.equals("company")){
			qtype = "会社について";
		}else if (qtype.equals("product")){
			qtype = "製品について";
		}else if (qtype.equals("support")){
			qtype = "アフターサポートについて";
		}

		if(body == null || body.length() == 0){
			errorMsg += "内容が入力されていません<br>";
		}


		response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out = response.getWriter();
		out.println("<html>");
		out.println("<head>");
		out.println("<title></title>");
		out.println("</head>");
		out.println("<body>");
		out.println("hello");
		out.println("<p>お名前："+name+"</p>");
		out.println("<p>お問い合わせの種類：+qtype+"</p>");
		out.println("<p>アフターサポートについて"+body+"</p>");
		out.println("<p>"+errorMsg+"</p>");
		out.println("</body>");
		out.println("</html>");
	}
}